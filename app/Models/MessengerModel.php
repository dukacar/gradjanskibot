<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\Messenger;

class MessengerModel extends BaseDataModel
{
    /**
     * Instantiate
     *
     * @return void
     */
    public function __construct()
    {
        self::$messenger_id = $this->getMessengerId();
    }

    /**
     * Get messenger id by messanger name
     *
     * @return integer
     */
    protected function getMessengerId()
    {
        if (!empty($this->messenger_id))
        {
            return $this->messenger_id;
        }

        return Messenger::where('name', $this->messenger_name)->value('id');
    }
}
