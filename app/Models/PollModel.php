<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\Participant;
use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Organization;
use App\Models\DbTables\Poll;
use App\Models\DbTables\ParticipantPoll;
use App\Models\DbTables\Queue;
//use App\Models\DbTables\ParticipantGroup;
//use App\Models\DbTables\Round;
//use App\Models\DbTables\Group;

//use App\Models\Helper;

use Paragraf\ViberBot\Bot;
use Paragraf\ViberBot\Model\ViberUser;
use Paragraf\ViberBot\TextMessage;
use Paragraf\ViberBot\Event\MessageEvent;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\Message;

use Log;

class PollModel extends MessengerModel
{
    /**
     * Message text received from user
     *
     * @var Array
     */
    protected $message_text = [];

    /**
     * Message buttons
     *
     * @var Array
     */
    protected $buttons = [];

    /**
     * Message text
     *
     * @var String
     */
    protected $message;

    protected $pending_pool_time = '-1 days';
    protected $pending_answer_time = '-1 days';

    /**
     * Instantiate
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        parent::__construct();

        // Ovde uzeti participanta, organizaciju...
        self::$participant = Participant::getParticipantById(self::$participant_id);
        self::$messenger_uid = ParticipantMessenger::getParticipantUidById(self::$messenger_id, self::$participant['id']);
        self::$organization_id = self::$participant->organization_id;
        self::$organization = Organization::getOrganizationById(self::$participant_id);
    }

    protected function validateQuestion()
    {
        if (empty(self::$request->message['text']))
        {
            $this->message = 'Anketno pitanje nije ispravno. :(';
            return false;
        }

        return true;
    }

    public function askGroup()
    {
        if (!$this->validateQuestion())
        {
            return;
        }

        // Save poll into DB
        $poll = new Poll;
        self::$poll_id = $poll->createPoll(self::$participant_id, self::$request->message['text']);

        // Question
        $this->message = 'Kojoj grupi želiš da pošaljem anketu?';

        // Option group
        $ActionBody = [
            'action'  => EventConstants::POLL_INIT,
            'type'    => 'private',
            'poll_id' => self::$poll_id
        ];
        $button = new Button('reply', json_encode($ActionBody), strtoupper(self::$organization['name']), 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // Option public
        $ActionBody = [
            'action'  => EventConstants::POLL_INIT,
            'type'    => 'public',
            'poll_id' => self::$poll_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Javno', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;
    }

    protected function validateType()
    {
        if (!Helper::isJson(self::$request->message['text']))
        {
            $this->message = 'Izabrana grupa nije ispravna. :(';
            return false;
        }

        return true;
    }

    public function processPollType()
    {
        if (!$this->validateType())
        {
            return;
        }

        // Get Poll request data
        $poll_request_data = json_decode(self::$request->message['text']);
        self::$poll_type = $poll_request_data->type;
        self::$poll_id = $poll_request_data->poll_id;

        // Update Poll type
        $poll = new Poll;
        $poll->updatePollTypeById(self::$poll_id, self::$poll_type);
        self::$poll_question = $poll->getPoolQuestionById(self::$poll_id);

        // Check if another pool is pending, ask to start new poll

        $this->sendPoll();

        // Question
        $this->message = 'Anketa je poslata.';

        return false;
    }

    protected function sendPoll()
    {
        // Option group
        $buttons = [];
        $ActionBody = [
            'poll_id' => self::$poll_id,
            'answer'  => 'agree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => self::$poll_id,
            'answer'  => 'disagree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => self::$poll_id,
            'answer'  => 'neutral'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Create message
        $keyboard = new Keyboard($buttons);
        $message = new Message('text', $keyboard);
        $message->setTrackingData(EventConstants::POLL_ANSWER);

        // Get recipients
        $participant = new Participant;
        if (self::$poll_type == 'private')
        {
            $recipients = $participant->getAllSubscribedParticipantsByOrganizationId(self::$organization_id);
        }
        else
        {
            $recipients = $participant->getAllSubscribedParticipants();
        }

        // Compile paricipant question
        $question = 'Odbornik ' . self::$participant['name'] . ' želi vaše mišljenje na pitanje:' .
        "\n" . "\n" .
        '"' . self::$poll_question . "'";

        $delivered = 0;
        $pending = 0;
        foreach ($recipients as $recipient)
        {
            // Check if participant has pending polls
            //$has_recent_pending =
            //    $participant->participantHasPendingPoll($recipient['id'], $this->pending_pool_time) ||
            //    $participant->participantHasPendingAnswer($recipient['id'], $this->pending_answer_time);

            $is_pending = Queue::isPrticipantPending($recipient['id']);

            $status = 'pending';
            $queue_status = 0;
            // Exclude sender as report needs to be sent first
            if (!$is_pending && $recipient['id'] !== self::$participant_id)
            {
                $status = 'sent';
                $queue_status = 1;
                $delivered ++;

                // Send poll to participant
                $this->sendPollToRecipient($recipient['messenger_uid'], $message, $question);
                //(new Bot(self::$request, $message))
                //->on(new MessageEvent(self::$request->timestamp, self::$request->message_token,
                //new ViberUser($recipient['messenger_uid'], self::$request->sender['name']), self::$request->message))
                //->replay($question)
                //->send();
            }
            else
            {
                $pending++;
            }

            // Save participant_poll record
            $participant_poll = new ParticipantPoll;
            $participant_poll->createParticipantPoll($recipient['id'], self::$poll_id, $status);

            // Save queue record
            $queue = new Queue;
            $queue->createQueue($recipient['id'], 'poll', self::$poll_id, $queue_status);
        }

        // Send report to sender
        $delivered ++; // Need to add sender to delivered
        $pending --; // Need to substract sender from pending
        $this->sendReportToSender($delivered, $pending);
        // Send poll to sender also
        $this->sendPollToRecipient(self::$messenger_uid, $message, $question);
        // Update sender queue
        $queue = new Queue;
        $queue->updateQueueStatus(self::$participant_id, 'poll', self::$poll_id);
        // Update participant_poll status for sender
        $participant_poll = new ParticipantPoll;
        $participant_poll->updatePollStatus(self::$poll_id, self::$participant_id, 'sent');
    }

    protected function sendPollToRecipient(string $messenger_uid, $message, string $question)
    {
        (new Bot(self::$request, $message))
            ->on(new MessageEvent(self::$request->timestamp, self::$request->message_token,
            new ViberUser($messenger_uid, self::$request->sender['name']), self::$request->message))
            ->replay($question)
            ->send();
    }

    protected function sendReportToSender(int $delivered, int $pending)
    {
        $message = new Message('text', NULL);
        $report = 'Vaša anketa na pitanje:' .
        "\n" .
        '"' . self::$poll_question . "'" .
        "\n" .
        'je posalta.' .
        "\n" . "\n" .
        'Broj isporučenih: ' . $delivered .
        "\n" .
        'Broj na čekanju: ' . $pending;

        (new Bot(self::$request, $message))
            ->on(new MessageEvent(self::$request->timestamp, self::$request->message_token,
            new ViberUser(self::$messenger_uid, self::$request->sender['name']), self::$request->message))
            ->replay($report)
            ->send();
    }

    public function processPollAnswer()
    {
        //if (!$this->validateAnswer())
        //{
        //    return;
        //}

        // Get Poll request data
        $poll_request_data = json_decode(self::$request->message['text']);
        self::$poll_id = $poll_request_data->poll_id;
        self::$poll_answer = $poll_request_data->answer;

        // Update participant pool answer
        $participant_poll = new ParticipantPoll;
        $affected = $participant_poll->updatePollAnswer(self::$poll_id, self::$participant_id, self::$poll_answer);

        // Update poll answer count
        Poll::updateAnswerCount(self::$poll_id, self::$poll_answer);

        // Delete participant from queue
        Queue::deleteQueue(self::$participant_id, 'poll', self::$poll_id);

        // Add poll_report for participant in queue
        $queue = new Queue;
        $queue->createQueue(self::$participant_id, 'poll_report', self::$poll_id);

        // Check if enough answered to send results to participants
        $sent = ParticipantPoll::getSentCount(self::$poll_id);
        $answered = ParticipantPoll::getAnsweredCount(self::$poll_id);

        $sent = !empty($sent) ? $sent : 1;
        if (round($answered / $sent * 100, 0) < EventConstants::POLL_RESULT_MARGIN)
        {
            // Poll report is not ready, send thank you to participant
            $this->sendVotedThanksToParticipant(self::$messenger_uid, $sent, $answered);

            // Check if there is new event for user
            $dispatcher = new DispatcherModel();
            $dispatcher->DispatchNext(self::$request, self::$participant_id);

            return true;
        }

        // Compile poll report
        self::$poll = Poll::getPollById(self::$poll_id);

        $report = 'Trenutni rezultat ankete na pitanje:' .
            "\n" .
            '"' . self::$poll->question . '"' .
            "\n" .
            'je sledeći:' .
            "\n" . "\n" .
            'ukupno učesnika: ' . $sent .
            "\n" .
            'glasalo: ' . $answered . ' (' . round($answered / $sent * 100, 2) . '%)' .
            "\n" . "\n" .
            'za: ' . self::$poll['agree'] . ' (' . round(self::$poll['agree'] / $answered * 100, 2) . '%)' .
            "\n" .
            'protiv: ' . self::$poll['disagree'] . ' (' . round(self::$poll['disagree'] / $answered * 100, 2) . '%)' .
            "\n" .
            'suzdržano: ' . self::$poll['neutral'] . ' (' . round(self::$poll['neutral'] / $answered * 100, 2) . '%)';
            //"\n" . "\n" .
            //'Jos neki info ovde.';

        // Get participants waiting for the poll report
        $recipients = Queue::getPrticipantsForType('poll_report', self::$poll_id);
        foreach ($recipients as $recipient)
        {
            // Check if participant has other event pending in queue
            $pending = Queue::isPrticipantPendingOtherType($recipient['participant_id'], 'poll_report', self::$poll_id);
            if (!$pending)
            {
                // Set queue status
                $queue = new Queue;
                $queue->updateQueueStatus($recipient['participant_id'], 'poll_report', self::$poll_id);
                // Send report
                $this->sendReportToParticipant($recipient['messenger_uid'], $report);
                // Delete from queue
                Queue::deleteQueue($recipient['participant_id'], 'poll_report', self::$poll_id);
            }
        }

        // Check if there is new event for user
        $dispatcher = new DispatcherModel();
        $dispatcher->DispatchNext(self::$request, self::$participant_id);

        return false;
    }

    protected function sendVotedThanksToParticipant(string $messenger_uid, int $sent, int $answered)
    {
        $message = new Message('text', NULL);

        $report = 'Hvala što ste glasali.' .
        "\n" . "\n" .
        'Čim ' . EventConstants::POLL_RESULT_MARGIN . '% građana odgovori na anketno pitanje, poslaću vam rezultate.' .
        "\n" . "\n" .
        'Trenutno stanje: ' .
        "\n" .
        'ukupno učesnika: ' . $sent .
        "\n" .
        'glasalo: ' . $answered . ' (' . round($answered / $sent * 100, 2) . '%)';

        (new Bot(self::$request, $message))
            ->on(new MessageEvent(self::$request->timestamp, self::$request->message_token,
            new ViberUser(self::$messenger_uid, self::$request->sender['name']), self::$request->message))
            ->replay($report)
            ->send();
    }

    protected function sendReportToParticipant(string $messenger_uid, string $report)
    {
        $message = new Message('text', NULL);

        (new Bot(self::$request, $message))
            ->on(new MessageEvent(self::$request->timestamp, self::$request->message_token,
            new ViberUser($messenger_uid, self::$request->sender['name']), self::$request->message))
            ->replay($report)
            ->send();
    }

    /**
     * Get message
     *
     * @return \Paragraf\ViberBot\Messages\Message
     */
    public function getMessage()
    {
        if (!empty($this->buttons))
        {
            $keyboard = new Keyboard($this->buttons);
            $message = new Message('text', $keyboard);
        }
        else
        {
            $message = new Message('text', NULL);
        }
        $message->setTrackingData(EventConstants::POLL);
        return $message;
    }

    /**
     * Get message text
     *
     * @return String
     */
    public function getMessageText()
    {
        return $this->message;
    }
}
