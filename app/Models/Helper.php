<?php

namespace App\Models;

class Helper
{
    /**
     * Check if string is JSON
     *
     * @param  String  $string  String to check
     * @return Bool
     */
    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
