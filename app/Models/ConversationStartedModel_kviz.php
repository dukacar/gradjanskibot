<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Representative;
use App\Models\DbTables\Organization;
use App\Models\DbTables\ParticipantGroup;
use App\Models\DbTables\Round;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\WelcomeMessage;

class ConversationStartedModel extends MessengerModel
{
    /**
     * Participant id
     *
     * @var Int
     */
    protected $participant_id;

    protected $welcome_buttons = [];
    protected static $welcome_buttons_type;
    protected $welcome_message;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        parent::__construct($request, $messenger_name);

        if (empty($this->request->context))
        {
            $this->qrCodeError();
            return;
        }

        $this->setContextData($this->request->context);

        $this->createParticipant();

        if (!empty($this->request->context))
        {
            $this->qrCodeParticipant();
        }
        else
        {
            $this->referredParticipant();
        }
    }

    protected function qrCodeError()
    {
        $this->welcome_message = 'Zdravo!' .
            "\n" .
            'Na žalost QR kod koji si skenirao nije validan. :(' .
            "\n\n" .
            'Ako želiš da se uključiš u odlučivanje u svojoj zajednici, molim te da pronađeš ispravan QR kod i pokušaš ponovo.' .
            "\n" .
            'Možeš da me obrišeš i neću te više uznemiravati.';
    }

    protected function setContextData($context)
    {
        // context: indjija|psg|odbornik

        $context_data = explode("|", $context);

        if (!empty($context_data[0]))
        {
            $this->municipality = $context_data[0];
        }

        if (!empty($context_data[1]))
        {
            $this->organization = $context_data[1];
        }

        $this->representative = false;
        if (!empty($context_data[2]) && $context_data[2] == 'odbornik')
        {
            $this->representative = true;
        }
    }

    /**
     * Create participant
     *
     * @return void
     */
    public function createParticipant()
    {
        // Note: participant can trigger conversion_started event more than once
        // so he might already exist in the DB and might has ongoing participation in the round

        $this->participant_id = ParticipantMessenger::getParticipantIdByUid($this->messenger_id, $this->request->user['id']);

        // Check if organization exists
        if (!empty($this->organization))
        {
            $this->organization_id = Organization::getOrganizationIdByMunicipalityAndName($this->municipality, $this->organization);
            if (empty($this->organization_id))
            {
                $this->createOrganization();
            }
        }

        if (empty($this->participant_id))
        {
            // Create representative record
            $representative = new Representative;
            $representative->name = $this->request->user['name'];
            $representative->organization_id = $this->organization_id;
            $representative->save();
            $this->participant_id = $representative->id;

            // Create participant_messenger record
            $participant_messenger = new ParticipantMessenger;
            $participant_messenger->participant_id = $this->participant_id;
            $participant_messenger->messenger_id = $this->messenger_id;
            $participant_messenger->messenger_uid = $this->request->user['id'];
            $participant_messenger->avatar = $this->request->user['avatar'];
            $participant_messenger->language = $this->request->user['language'];
            $participant_messenger->county = $this->request->user['country'];
            // $participant_messenger->subscribed = $subscribed;
            // $participant_messenger->unsubscribed = $unsubscribed;
            $participant_messenger->api_version = $this->request->user['api_version'];
            $participant_messenger->save();
            $participant_messenger_id = $participant_messenger->id;
        }
    }

    /**
     * Process not referred paricipant, QR code is scanned
     *
     * @return string
     */
    public function qrCodeParticipant()
    {
        if (!empty($this->representative))
        {
            $this->welcome_message = 'Zdravo ' . $this->request->user['name'] . '!' .
                "\n" .
                'Drago mi je da te ponovo vidim.' .
                "\n\n" .
                'Moja misija je da ti omogućim direktnu komunikaciju sa građanima.' .
                "\n\n" .
                'Omogućiću ti da praviš ankete i odgovaraš na pitanja građana.' .
                "\n" .
                'Možeš da me obrišeš i neću te više uznemiravati (osim kao te neki prijatelj ne uključi u kviz rundu).';

            $this->welcome_buttons = self::getQrCodeContinueButtons();
        }
        else
        {
            $this->welcome_message = 'Zdravo!' .
                "\n" .
                'Dobrodošao na Kviz Bot.' .
                "\n\n" .
                'Ako želiš da počneš klikni na dugme dole.' .
                "\n" .
                'Ako ne, obriši me, neću te više uznemiravati (osim kao te neki prijatelj ne uključi u kviz rundu).';

            $this->welcome_buttons = self::getQrCodeNewButtons();
        }

        $this->welcome_buttons[] = $this->getInstructionsButton();
    }

    public static function getQrCodeContinueButtons()
    {
        $buttons = [];

        // Continue with group
        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::CONTINUE_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Želim da nastavim.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        // Opt out from group
        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::NEW_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Isključi me iz grupe.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        self::$welcome_buttons_type = __FUNCTION__;

        return $buttons;
    }

    public static function getQrCodeNewButtons()
    {
        $buttons = [];

        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::NEW_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Želim da probam.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        self::$welcome_buttons_type = __FUNCTION__;

        return $buttons;
    }

    /**
     * Process referred participant, we need to join him to a group
     *
     * @return string
     */
    public function referredParticipant()
    {

    }

    /**
     * Check if user is participating in any ongoing round
     *
     * @return Bool
     */
    protected function isParticipantInActiveRound()
    {
        // Get groups where paricipant is active
        $participant_group_ids = ParticipantGroup::getParticipantGroupsActive($this->participant_id);
        if (empty($participant_group_ids))
        {
            // Participant doesn't belong to any group (thus no round started)
            return false;
        }

        // Check if there is not finished round for the group
        foreach ($participant_group_ids as $participant_group_id)
        {
            if (Round::hasActiveRoundsForGroup($participant_group_id))
            {
                return true;
            }
        }

        return true;
    }

    /**
     * Get instructions button
     *
     * @return Paragraf\ViberBot\Model\Button
     */
    protected function getInstructionsButton()
    {
        $ActionBody = [
            'action'  => EventConstants::INSTRUCTIONS,
            'buttons' => self::$welcome_buttons_type
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Pokaži mi uputstvo.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');

        return $welcome_button;
    }

    /**
     * Get welcome message
     *
     * @return \Paragraf\ViberBot\Messages\WelcomeMessage
     */
    public function getWelcomeMessage()
    {
        $welcome_keyboard = new Keyboard($this->welcome_buttons);
        return new WelcomeMessage('text', $welcome_keyboard, $this->welcome_message);
    }
}
