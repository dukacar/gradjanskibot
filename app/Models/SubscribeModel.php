<?php

namespace App\Models;

//use Illuminate\Http\Request;
use App\Models\DbTables\ParticipantMessenger;

use Log;

class SubscribeModel extends MessengerModel
{
    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set participant subscribed
     *
     * @return void
     */
    public function setSubscribed()
    {
        $request = self::$request->request->all();

        $participant_id = !empty($request['sender']['id']) ? $request['sender']['id'] : '';
        if (empty($participant_id))
        {
            $participant_id = !empty($request['user']['id']) ? $request['sender']['id'] : '';
        }

        if (empty($participant_id))
        {
            Log::info(
                'UNSUBSCRIBE ERROR!' . PHP_EOL .
                'Incoming Viber API POST request: ' . PHP_EOL .
                'request: ' . PHP_EOL . print_r($request, true) . PHP_EOL
            );
        }

        $participant_messenger = ParticipantMessenger::where('messenger_id', self::$messenger_id)
            ->where('messenger_uid', $request['sender']['id'])
            ->first();

        $participant_messenger->subscribed = time();
        $participant_messenger->unsubscribed = 0;

        $participant_messenger->save();
    }

    /**
     * Set participant unsubscribed
     *
     * @return void
     */
    public function setUnsubscribed()
    {
        $participant_messenger = ParticipantMessenger::where('messenger_id', self::$messenger_id)
            ->where('messenger_uid', self::$request->user_id)
            ->first();

        // Might be deleted participant for some reason
        if (!empty($participant_messenger))
        {
            $participant_messenger->subscribed = 0;
            $participant_messenger->unsubscribed = time();

            $participant_messenger->save();
        }
    }
}
