<?php

namespace App\Models;

class EventConstants
{
    const REPREZENTATIVE = 'reprezentative';
    const CITIZEN = 'citizen';

    const ONBOARDING = 'onboarding';
    const ONBOARDING_QUESTION = 'onboarding_question';
    const NEW_ROUND = 'new';
    const PROCEED = 'continue';
    const CONTINUE_ROUND = 'continue';
    const INSTRUCTIONS = 'instructions';
    const DIFFICULTY = 'difficulty';
    const QUESTIONS = 'questions'; // Questions count
    const START = 'start'; // Questions count

    const POLL = 'poll';
    const POLL_INIT = 'poll_init';
    const POLL_ANSWER = 'poll_answer';

    const POLL_RESULT_MARGIN = 100;
}
