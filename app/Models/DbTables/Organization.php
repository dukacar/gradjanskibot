<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organization';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    protected static $name;
    protected static $municipality;

    /**
     * Get organization by organization id
     *
     * @param  Integer  $organization_id  Organization id
     * @return Organization object
     */
    public static function getOrganizationById(int $organization_id)
    {
        return self::where('id', $organization_id)
            ->first();
    }

    /**
     * Get organization id by municipality and name
     *
     * @param  string  $municipality  Municipality
     * @param  string  $name          Organization name
     * @return int
     */
    public static function getOrganizationIdByMunicipalityAndName(string $municipality, string $name)
    {
        $organization_id = self::where('municipality', $municipality)
            ->where('name', $name)
            ->value('id');

        if (empty($organization_id))
        {
            $organization_id = 0;
        }

        return $organization_id;
    }

    /**
     * Delete all cascading data by paricipant group id
     *
     * @param  int  $group_id  Group id
     * @return bool
     */
    public static function deleteGroupById(int $group_id)
    {
        self::where('id', $group_id)->delete();
    }

    /**
     * Create a new group
     *
     * @param  string  $group_name  Group name
     * @return int Group id
     */
    public function createGroup(string $group_name = '', string $municipality = '')
    {
        // $this->name = $group_name;
        self::$name = $name;
        self::$municipality = $municipality;
        $this->save();
        return $this->id;
    }
}
