<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'representative';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
}
