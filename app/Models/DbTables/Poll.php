<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'poll';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Get poll by poll id
     *
     * @param  Integer  $poll_id  Poll id
     * @return Poll object
     */
    public static function getPollById(int $poll_id)
    {
        return self::where('id', $poll_id)
            ->first();
    }

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createPoll(int $reprezentative_id, string $question = '')
    {
        $this->reprezentative_id = $reprezentative_id;
        $this->question = $question;
        $this->save();
        return $this->id;
    }

    /**
     * Update poll type by id
     *
     * @param  int     $poll_id  Poll id
     * @param  string  $type     Poll type
     * @return void
     */
    public function updatePollTypeById(int $poll_id, string $type = 'public')
    {
        $this->where('id', $poll_id)
            ->update(['type' => $type, 'status' => 'started']);

        return $this;
    }

    /**
     * Get poll question by poll id
     *
     * @param  Integer  $poll_id  Poll id
     * @return Organization object
     */
    public static function getPoolQuestionById(int $poll_id)
    {
        return self::where('id', $poll_id)
            ->value('question');
    }

    /**
     * Update poll answer count
     *
     * @param  Integer  $poll_id  Poll id
     * @return Organization object
     */
    public static function updateAnswerCount(int $poll_id, string $answer)
    {
        return self::where('id', $poll_id)
            ->increment($answer, 1);
    }
}
