<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_answer';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Check if participant has recent pending answer
     *
     * @param  Integer  $participant_id     Participant id
     * @param  String   $pending_answer_time  Pending pool offset time
     * @return Bool
     */
    public static function participantHasPendingAnswer(int $participant_id, string $pending_answer_time = '-1 days')
    {
        return (bool) self::where('participant_id', '=', $participant_id)
            ->where('status', '=', 'sent')
            ->where('sent_time', '>', strtotime($pending_answer_time))
            ->count();
    }

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createParticipantAnswer(int $reprezentative_id, string $question = '')
    {
        $this->reprezentative_id = $reprezentative_id;
        $this->question = $question;
        $this->save();
        return $this->id;
    }

    /**
     * Get organization by organization id
     *
     * @param  Integer  $organization_id  Organization id
     * @return Organization object
     */
    public static function getParicipantPollById(int $poll_id)
    {
        return self::where('id', $poll_id)
            ->first();
    }

    /**
     * Update poll type by id
     *
     * @param  int     $poll_id  Poll id
     * @param  string  $type     Poll type
     * @return void
     */
    public function updatePollTypeById(int $poll_id, string $type = 'public')
    {
        $this->where('id', $poll_id)
            ->update(['type' => $type, 'status' => 'started']);

        return $this;
    }
}
