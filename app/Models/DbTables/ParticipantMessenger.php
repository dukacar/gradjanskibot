<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantMessenger extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_messenger';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    public function participant()
    {
        return $this->belongsTo('Participant');
    }

    /**
     * Get participant id by messenger user id
     *
     * @param  int     $messenger_id   Messenger id
     * @param  string  $messenger_uid  Messenger user id
     * @return int
     */
    public static function getParticipantIdByUid(int $messenger_id, string $messenger_uid)
    {
        return self::where('messenger_id', $messenger_id)
            ->where('messenger_uid', $messenger_uid)
            ->value('id');
    }

    /**
     * Get participant uid by messenger user id
     *
     * @param  int     $messenger_id   Messenger id
     * @param  string  $messenger_uid  Messenger user id
     * @return int
     */
    public static function getParticipantUidById(int $messenger_id, string $participant_id)
    {
        return self::where('messenger_id', $messenger_id)
            ->where('participant_id', $participant_id)
            ->value('messenger_uid');
    }
}
