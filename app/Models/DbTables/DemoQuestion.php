<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

use DB;

class DemoQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demo_question';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createDemoQuestion(int $participant_id, string $question)
    {
        $this->participant_id = $participant_id;
        $this->question = $question;
        $this->save();
        return $this->id;
    }
}
