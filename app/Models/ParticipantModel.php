<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Participant;

use App\Models\DbTables\Messenger;

class ParticipantModel extends MessengerModel
{
    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        parent::__construct();
        self::$participant_id = $this->getParticipantIdByUid();
    }

    /**
     * Get participant by participiant's messenger user id
     *
     * @return Int or Null
     */
    protected function getParticipantUid()
    {
        return ParticipantMessenger::where('messenger_id', $this->messenger_id)
            ->where('messenger_uid', $this->request->user['id'])
            ->value('id');
    }

    /**
     * Get participant id by participiant's messenger user id
     *
     * @return Int or Null
     */
    protected function getParticipantIdByUid()
    {
        return ParticipantMessenger::where('messenger_id', $this->messenger_id)
            ->where('messenger_uid', $this->request->user['id'])
            ->value('id');
    }

    /**
     * Get participant type by context data
     *
     * @param  String  $rganization     Organization name
     * @param  Bool    $representative  Is representative
     * @return String
     */
    public static function getParticipantTypeByContextData(string $rganization, bool $representative)
    {
        if ($representative)
        {
            return 'reprezentative';
        }
        else if (!empty($rganization))
        {
            return 'affiliated';
        }
        else
        {
            return 'unaffiliated';
        }
    }
}
