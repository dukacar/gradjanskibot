<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Participant;
use App\Models\DbTables\DemoAnswer;
use App\Models\DbTables\DemoQuestion;
use App\Models\DbTables\DemoFinished;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\Message;

use Log;

class OnboardingModel extends MessengerModel
{
    /**
     * Message text received from user
     *
     * @var Array
     */
    protected $message_text = [];

    /**
     * Group id
     *
     * @var Int
     */
    protected $step;
    protected $type;
    protected $step_pitanje = false;
    protected $pitanje = '';
    protected $finished = false;

    /**
     * Participant group id
     *
     * @var Int
     */
    protected $participant_group_id;

    /**
     * Round id
     *
     * @var Int
     */
    protected $round_id;

    /**
     * Message buttons
     *
     * @var Array
     */
    protected $buttons = [];

    /**
     * Message text
     *
     * @var String
     */
    protected $message;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        parent::__construct();

        // Mozda proveriti input !!!!!!!!!
//$tracking = self::$request->message['tracking_data'];
//echo "this->request->message['tracking_data']: '$tracking'<br>";
//dd(self::$request);

        // Set participant id
        self::$participant_id = ParticipantMessenger::getParticipantIdByUid(
            self::$messenger_id,
            self::$request->sender['id']
        );

        // Get participant
        // self::$participant = Participant::getParticipantById(self::$participant_id);

        if (self::$request->message['tracking_data'] == EventConstants::ONBOARDING_QUESTION)
        {
            // Save demo question
            $demo_question = new DemoQuestion;
            $demo_question->createDemoQuestion(self::$participant_id, self::$request->message['text']);

            // Zadaj step i type
            $this->step = 11;
            $this->type = EventConstants::CITIZEN;
        }
        else if (self::$request->message['tracking_data'] == EventConstants::ONBOARDING)
        {
            $this->message_text = json_decode((string) self::$request->message['text']);

            if (!empty($this->message_text->step))
            {
                $this->step = $this->message_text->step;
                $this->type = !empty($this->message_text->type) ? $this->message_text->type : EventConstants::CITIZEN;

                if ($this->step == 'finished')
                {
                    $this->finished = true;
                    $this->sendFinished();
                    return;
                }
            }
            else
            {
                if (empty($this->message_text->error))
                {
                    $this->sendError();
                    return;
                }
                else
                {
                    $this->step = Participant::getLastDemoStep(self::$participant_id);
                    $this->step = !empty($this->step) ? $this->step : 1;
                }
            }

            if ($this->step == 2 && $this->message_text->answer == 'agree')
            {
                $this->step++;
            }
        }
        else
        {
            // Ovdre za nepredvidjene situacije
        }

        // Save onboarding step
        Participant::updateOnboardingStep(self::$participant_id, $this->step);

        if ($this->step == 19)
        {
            $subscribe = new SubscribeModel();
            $subscribe->setSubscribed();

            Participant::updateOnboardingStep(self::$participant_id, 100);

            $demo_finished = new DemoFinished;
            $demo_finished->createDemoFinished(self::$participant_id);
        }

        $show_step = 'step_' . $this->step;
        $this->$show_step();

        // Ako je step 20, subscribe
        // Postavi onboarding step na 100

        /*
        switch ($this->message_text->action) {
            case 'instructions':
                return $this->sendInstructions($this->message_text->buttons);
                break;
            case 'continue':
                $this->setPropertiesFromMessageText();
                return $this->sendInstructions($this->message_text->type);
                //return $this->continueOnboarding($this->message_text->step);
                break;
            default:
                Log::info(
                    'Message can not be processed: ' . $this->request->message['taxt'] . PHP_EOL .
                    'Incoming Viber API POST request: ' . PHP_EOL .
                    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                );
        }
        */
    }

    /**
     * Compile instruction message
     *
     * @param  String  $buttons  ConversationStartedModel method name to get buttons
     * @return void
     */
    protected function sendInstructions(string $type)
    {
        if ($type == 'representative')
        {
            return $this->sendInstructionsRepresentative();
        }

        return $this->sendInstructionsCitizen();
    }

    /**
     * Compile instruction message for the representative
     *
     * @return void
     */
    protected function sendInstructionsRepresentative()
    {
        $this->message = 'Kratko uputstvo za korišćenje odborniku.';
    }

    /**
     * Oboarding error
     *
     * @return void
     */
    protected function sendError()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Eh sad... (cool)' .
                "\n" .
                'Ovo je samo DEMONSTRACIONA verzija kreirana da do izbora vidiš šta smo to smislili.' .
                "\n" .
                'Funkcionalnost je trenutno ograničena jer i nema odbornika kojima bi poslao pitanje (flirt)' .
                "\n\n" .
                'Grasaj za nas na lokalnim izborima i naši odbornici će odgovarati na tvoja i pitanja svih zainteresovanih građana.' .
                "\n" .
                'Ali znaj da je naš je cilj da svi odbornici koriste ovu platformu.';

            $ActionBody = [
                'error'  => 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'OK! Nastavi sa prezentacijom.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding finished
     *
     * @return void
     */
    protected function sendFinished()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Hvala na strpljenju.' .
                "\n" .
                'U narednim danima ću ti slati demonstracione ankete i informacije.' .
                "\n\n" .
                'Šri dalje!';
        }
    }

    /**
     * Oboarding step 1
     *
     * @return void
     */
    protected function step_1()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Ja sam softver.' .
                "\n\n" .
                'Egzistiram u "oblaku" (što je samo naziv za tamo neki kompjuter na Internetu) i postojim dok građani misle da sam im potreban.' .
                "\n" .
                'Trenutni broj prijavljenih gradjana kojima služim je 1334.' .
                "\n\n" .
                'Iako je skup pravila koje moram da postujem mali i prost ipak mi je dužnost da te informišem o tome kako funkcionišem.' .
                "\n\n" .
                'Ovo je samo DEMONSTRACIONA verzija kreirana da do izbora vidiš šta smo to smislili.' .
                "\n\n" .
                'Da li želiš ta ti pokažem kako ću ti koristiti?' .
                "\n\n" .
                'Tvoja pitanja i odgovori neće biti poslata nikome.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'   => 'agree',
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Želim.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'   => 'disagree',
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Ne želim.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 2
     *
     * @return void
     */
    protected function step_2()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = ':(' .
                "\n" .
                'Ja nisam klasična grupa u kojoj će te svi zatrpavati bespotrebnim informacijama.' .
                "\n\n" .
                'Ako si siguran, obriši me kao bilo koju grupu. Izaberi "delete and unsubscribe" opciju i neću te više uznemiravati.' .
                "\n" .
                'Ako se kasnije predomisliš, uvek ponovo možeš skenirati QR kod i bićes dobrodošao.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Hajde dobro, ipak mi objasni.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 3
     *
     * @return void
     */
    protected function step_3()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Hvala ti!' .
                "\n\n" .
                'Prvo moram da ti kažem da ću rasti sa brojem građana kojima služim.' .
                "\n" .
                'Što se više građana priključi to ćete zajedno biti jači i povratićete deo moći u svoje ruke ( ili prste, posto tipkate prstima. ;) )' .
                "\n" .
                'Ozbiljno ću prihvatiti sve vaše sugestije i prilagođavaću se vašim potrebama.' .
                "\n\n" .
                'Iako sam po prirodi zafrkant, videćeš da u vezi mene nema ničeg neozbiljnog i neumorno ću raditi za tebe.' .
                "\n" .
                'Ne moram da idem u toalet, nikada ne spavam i ne mogu biti zaražen Korona virusom.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Dobro, hajde vise, nastavi!', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 4
     *
     * @return void
     */
    protected function step_4()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Tvoji sugrađani smatraju da odbornici često ne zastupaju interes svojih birača u dovoljnoj meri i imaju potrebu da komuniciraju sa odbornicima na lak i efikasan način, na obostranu korist.' .
                "\n\n" .
                'Moji tvorci su tvoji sugrađani iz nestranačke inicijative i njihova misija je da poboljšaju komunikaciju građana i odbornika kao i da građanima omoguće veći broj informacija o radu lokalne samouprave' .
                "\n\n" .
                'Ova platforma omogućava :' .
                "\n\n" .
                '- građanima da ih informise o planovima i odlukama lokalne samouprave' .
                "\n" .
                '- lokalnoj samoupravi informaciju i uvid u to šta građani misle o tim planovima i odlukama' .
                "\n" .
                '- građanima da iskažu svoj stav i mišljenje o važim pitanjima u svojoj zajednici' .
                "\n" .
                '- lokalnoj samoupravi uvid u to koji su problemi građana i na koji prihvatljiv način mogu da ih reše ';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Kako?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 5
     *
     * @return void
     */
    protected function step_5()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Odbornici će meni slati informacije a ja ću ih zatim prosleđivati tebi i svim sugrađanima koji su uključeni.' .
                "\n" .
                'Informacije će ti biti predstavljene u vidu anketa, da ne kažem: neke vrste referenduma.' .
                "\n" .
                'Odbornik će definisati vreme trajanja ankete.' .
                "\n\n" .
                'Kad god odbornik pošalje neku informaciju, pored njegovog imena ću staviti emotikon koji će opisivati njegovu aktivnost:' .
                "\n" .
                '(smiley) - aktivan' .
                "\n" .
                '(straight) - može to i bolje' .
                "\n" .
                '(sad) - slabo je to gospodine/gospođice' .
                "\n" .
                'Emotikon ću svaki put izračunavati po broju poena tog odbornika u odnosu na poene ostalih odbornika.' .
                "\n\n" .
                'Čim glasaš, poslaću ti trenutno stanje ankete.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Daj da vidim i to cudo.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 6
     *
     * @return void
     */
    protected function step_6()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Odbornik [ime odbornika] (smiley) pita te za mišljenje:' .
                "\n\n" .
                '"Da li misliš da će se većini tvojih sugrađana svideti ova ideja?"';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'agree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'disagree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'neutral'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 7
     *
     * @return void
     */
    protected function step_7()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            // Save demo answer
            $demo_answer = new DemoAnswer;
            $this->message_text->answer = !empty($this->message_text->answer) ? $this->message_text->answer : 'error!';
            $demo_answer->createDemoAnswer(self::$participant_id, $this->message_text->answer);

            $this->message = 'Hvala što ste glasali.' .
                "\n\n" .
                'Čim 50% građana odgovori na anketno pitanje, poslaću vam rezultate.' .
                "\n\n" .
                'Trenutno stanje: .' .
                "\n" .
                'ukupno učesnika: 1334.' .
                "\n" .
                'glasalo: 522 (39.13%).';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Pokazi mi rezultate.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 8
     *
     * @return void
     */
    protected function step_8()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Trenutni rezultati ankete odbornika [ime odbornika] :) na pitanje:' .
                "\n" .
                '"Da li misliš da će se većini tvojih sugrađana svideti ova ideja?"' .
                "\n" .
                'je sledeći:' .
                "\n\n" .
                'ukupno učesnika: 1334' .
                "\n" .
                'glasalo: 667 (50.00%)' .
                "\n\n" .
                'za: 300 (44.98%)' .
                "\n" .
                'protiv: 203 (30.44%)' .
                "\n" .
                'suzdržano: 164 (24.59%)' .
                "\n\n" .
                'Anketa je trajala 1 dan.' .
                "\n\n" .
                'Kada istekne vreme trajanja ankete, poslaću ti konačne rezultate u istom formatu.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Dobro, kako ja da postavim pitanje?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 9
     *
     * @return void
     */
    protected function step_9()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Napominjem ti ponovo da su svi građani potpuno anonimni, niko, čak ni ja neće znati tvoj identitet i to mi je jako važno.' .
                "\n\n" .
                'Pitanje postavljaš tako što ga ukucaš kao i bilo koju poruku a ja ću ti zatim ponuditi da izabereš odbornika kome si namenio pitanje ali da ti prvo objasnim proces...' .
                "\n\n" .
                'Kada postaviš pitanje, neću ga poslati direktno odborniku nego svim građanima da ocene da li tvoje pitanje ima smisla.' .
                "\n\n" .
                'Ako većina od 10% građana oceni da tvoje pitanje ima smisla, poslaću pitanje odborniku a tebi ću poslati obaveštenje o tome.' .
                "\n\n" .
                'Slobodno postavi pitanje, samo ukucaj i pošalji.';

            $this->step_pitanje = true;
        }
    }

    /**
     * Oboarding step 10
     *
     * @return void
     */
    protected function step_10()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Izaberi odbornika od kojeg očekujes odgovor.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'reprezentative_id'  => 1
            ];
            $button = new Button('reply', json_encode($ActionBody), '[ime odbornika 1]', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'reprezentative_id'  => 2
            ];
            $button = new Button('reply', json_encode($ActionBody), '[ime odbornika 2]', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'reprezentative_id'  => 3
            ];
            $button = new Button('reply', json_encode($ActionBody), '[ime odbornika 3]', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 11
     *
     * @return void
     */
    protected function step_11()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Poslao sam tvoje pitanje odborniku [ime odbornika]. :)' .
                "\n\n" .
                'Broj građana koji je ocenio tvoje pitanje je 134.' .
                "\n\n" .
                'Pozitivnih: 89 (66.42%).' .
                "\n" .
                'Negativnih: 45 (33.58%).';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'I kada odbornik odgovori?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 12
     *
     * @return void
     */
    protected function step_12()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Obornik [ime odbornika 2] :) je odgovorio na tvoje pitanje:' .
                "\n" .
                '"Ovde će biti ispisano postavljeno pitanje".' .
                "\n\n" .
                'Odgovor odbornika:' .
                "\n" .
                '"Ovo je izmišljeni demo odgovor ali se nadamo da će odbornici ozbiljno shvatiti svoju ulogu i odgovarati sa dužnim poštovanjem."' .
                "\n\n" .
                'Oceni odgovor odbornika.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'agree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'disagree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'neutral'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 13
     *
     * @return void
     */
    protected function step_13()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Takođe ću odgovor odbornika na tvoje pitanje poslati svima na ocenu.' .
                "\n\n" .
                'Hoćeš da vidiš kako će izgledati kada neko drugi postavi pitanje odborniku?';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Naravno.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 14
     *
     * @return void
     */
    protected function step_14()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Sugrađanin je postavio pitanje odborniku [ime odbornika] :(' .
                "\n\n" .
                '"Kada opštinka uprava misli da uvede elektronske referendume za građane koji bi mogli da se dešavaju neretko i na razna pitanja?"' .
                "\n\n" .
                'Oceni pitanje.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'agree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'disagree'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            // Option public
            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
                'answer'  => 'neutral'
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 15
     *
     * @return void
     */
    protected function step_15()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Kada odbornik odgovori na pitanje sugrađana, poslaću odgovor i tebi na ocenu.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'To je to?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 16
     *
     * @return void
     */
    protected function step_16()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Nije. Postoji još jedno pravilo.' .
                "\n\n" .
                'Kada dobiješ anketu ili da oceniš smislenost nečijeg pitanja, neću ti slati više ništa dok ne daš svoje mišljenje ili dok anketa ili nečije pitanje ne istekne.' .
                "\n" .
                'Takođe, odbornik neće moći da šalje informacije dok ne odgovori na pitanje gradjana, ako pitanje istekne, "nagradiću" odbornika negativnim poenima.' .
                "\n\n" .
                'Moja svrha nije zabava već da budem oruđe u rukama građana kojim će ostvarivati svoje interese.' .
                "\n\n" .
                'Ni na koji način neću cenzurisati nikoga ali ne volim psovke i možda neću prenositi takve poruke nego ću zahtevati da se poruka preformuliše.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Da li će se pravila menjati?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 17
     *
     * @return void
     */
    protected function step_17()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Možda, ovo je demokratska platforma i ako građani zahtevaju drugačija pravila, ja ću se prilagoditi.' .
                "\n\n" .
                'Možda te budem pitao u kom kraju živiš da ne bi dobijao pitanja koja te ne interesuju.' .
                "\n" .
                'Možda te u nekom trenutku budem pitao da poodeliš svoju lokaciju sa mnom kako bi smo zajedno sprečili zloupotrebu.' .
                "\n\n" .
                'Za sada sam samo na viberu ali je plan da služim građanima na Fejsbuk mesindžeru i ostalim platformama koje svakodnevno koriste i na koje su navikli.' .
                "\n" .
                'Ako građani prihvate ovu ideju, dobiću i svoju mobilnu i web aplikaciju sa mnogo više mogućnosti.' .
                "\n\n" .
                'Moja uloga je da balansiram količinu informacija koju svi učesnici dobijaju i njihovu svrsishodnost u opštem interesu.';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Kakav je dalje plan?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 18
     *
     * @return void
     */
    protected function step_18()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Rodio sam se tek prošle nedelje ali kako budem rastao tako cu slušati vas građane i nuditi vam nove opcije koje budete zahtevali.' .
                "\n\n" .
                'Ako misliš da je ovo dobra ideja, nemoj me brisati a ja ću ti povremeno slati prave ankete da vidiš kako će sve ovo realno izgledati.' .
                "\n\n" .
                'Skeniraj QR kod i ponovo ću te provesti kroz funcionalnost.' .
                "\n\n" .
                'Za više informacija poseti moj sajt: https://gradjanskibot.automatica.rs';

            $ActionBody = [
                'step'   => $this->step + 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Kako mogu da pomognem?', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Oboarding step 19
     *
     * @return void
     */
    protected function step_19()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Ako zajedno uspemo da nateramo odbornike da me koriste možda me jednog dana, kada porastem, grad usvoji i učini oficijelnim alatom za participaciju građana u odlučivanju o svom gradu.' .
                "\n" .
                'Kada se to desi, opet ćete biti ponosni na svoj grad i svoje sugrađane kao nekada davno, sećate se?' .
                "\n\n" .
                'Mene ne interesuju stranke i politika, ja sam tu za vas, građane.' .
                "\n\n" .
                'Za sada će kandidati za odbornike "Nestranačkih komšijskih inicijativa", ako dobiju dovoljno vaših glasova na predstojećim izborima, na svom primeru pokazati kako sve može.' .
                "\n" .
                'Bilo koji odbornik koji želi da se priključi biće dobrodošao.' .
                "\n" .
                'Ako im pomogneš da uđu u skupštinu grada kao odbornici, biće tvoje "oči i uši", informisaće te o planovima i odlukama koje možda i nisu u skladu sa interesima građana.' .
                "\n\n" .
                'Oni misle da je ovo dobar način za zadobijanje poverenja i ako se ispostavi da je to tačno, samo je pitanje vremena kada će drugi obornici početi da rade za građane na isti način.' .
                "\n\n" .
                'Ako ti se ova ideja dopada, potreban nam je i tvoja podrška potpisom za našu listu, čekamo te.' .
                "\n\n" .

                'Stari tržni centar Sloboda, lokal 22, prvi sprat (preko puta kafića Versus).' .
                "\n" .
                '- Sreda, 3. jun, od 17 do 20 časova.' .
                "\n" .
                '- Četvrtak 4. jun, od 17 do 20 časova.' .
                "\n" .
                '- Petak 5. jun, od 16 do 20 časova.' .
                "\n" .
                '- Subota 6. jun, od 9 do 17 časova.' .
                "\n" .
                '- Od 16 do 20 časova.' .
                "\n\n" .
                'To je za sada to, kuckamo se. ;)';
        }
    }

    /**
     * Oboarding step 19
     *
     * @return void
     */
    protected function step_100()
    {
        if ($this->type == EventConstants::REPREZENTATIVE)
        {
        }
        else
        {
            $this->message = 'Demonstracija je gotova.' .
                "\n" .
                'Ako želiš da te provedem kroz demonstraciju, ponovo se priključi ili upotrebi dugme ispod.';

            $ActionBody = [
                'step'   => 1,
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Demonstriraj ponovo.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;

            $ActionBody = [
                'step'   => 'finished',
                'type'   => EventConstants::CITIZEN,
            ];
            $button = new Button('reply', json_encode($ActionBody), 'Sve mi je jasno.', 'regular');
            $button->setColumns(6);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Set class properties from message text received from user input
     *
     * @return void
     */
    protected function setPropertiesFromMessageText()
    {
        // Set participant id
        self::$participant_id = ParticipantMessenger::getParticipantIdByUid(
            $this->messenger_id,
            $this->request->sender['id']
        );

        // Set round id
        if (!empty($this->message_text->round_id))
        {
            $this->round_id = $this->message_text->round_id;
        }
    }

    /**
     * Compile instruction message
     *
     * @return void
     */
    public function continueOnboarding()
    {
        switch ($this->message_text->step) {
            case EventConstants::PROCEED:
                //$this->createNewRound();
                //return $this->askDifficulty();
                return $this->sendInstructions($this->message_text->buttons);
                break;
            case EventConstants::CONTINUE_ROUND:
                return $this->sendContinueRound(); // !!!!!!!!!!!!!!!!!!!!!
                break;
            case EventConstants::DIFFICULTY:
                $this->saveDifficulty();
                return $this->askQuestionsCount();
                break;
            case EventConstants::QUESTIONS:
                $this->saveQuestionsCount();
                $this->startRound();
                break;
            case EventConstants::START:
                die();
                //$this->saveQuestionsCount();
                //$this->startRound();
                break;
            default:
                Log::info(
                    'Onboarding can not be processed: ' . $this->request->message['text'] . PHP_EOL .
                    'Incoming Viber API POST request: ' . PHP_EOL .
                    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                );
        }
    }

    /**
     * Delete existing paricipants round(s) with cascading data and create new
     *
     * @return void
     */
    protected function createNewRound()
    {
        // pokupiti sve grupe gde je participant active
        $participant_group_ids = ParticipantGroup::getParticipantGroupsActive(self::$participant_id);
        Log::info(
            'Participant Groups (' . $this->participant_id . '): ' . PHP_EOL .
            'session: ' . PHP_EOL . print_r($participant_group_ids, true) . PHP_EOL .
            'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
        );

        foreach ($participant_group_ids as $participant_group_id)
        {
            if (ParticipantGroup::isSingleGroup($participant_group_id))
            {
                Round::deleteGroupRounds($participant_group_id);
                ParticipantGroup::deleteParticipantGroup($participant_group_id);
            }
            else
            {
                // Deactivate participant in group
                ParticipantGroup::deactivateParticipant($this->participant_id, $participant_group_id);
            }
        }

        // Create new group
        $group = new Group;
        $this->group_id = $group->createGroup($this->request->sender['name']);
        // Create new participant_group
        $participant_group = new ParticipantGroup;
        $this->participant_group_id = $participant_group->createParticipantGroup($this->participant_id, $this->group_id);
        // Create a new round
        $round = new Round;
        $this->round_id = $round->createRound($this->participant_group_id);
    }

    /**
     * Compile difficulty message
     *
     * @return void
     */
    protected function askDifficulty()
    {
        $this->message = 'Izaberi tezinu za novu rundu.';

        $buttons = [];

        // Easy
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'easy',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Lako', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // Medium
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'medium',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Osrednje', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // Hard
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'hard',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Teško', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;
    }

    protected function sendContinueRound()
    {

    }

    /**
     * Save difficulty
     *
     * @return void
     */
    protected function saveDifficulty()
    {
        Round::updateDifficulty($this->message_text->round_id, $this->message_text->difficulty);
    }

    /**
     * Compile questions count message
     *
     * @return void
     */
    protected function askQuestionsCount()
    {
        $this->message = 'Izaberi broj pitanja za novu rundu.';

        $buttons = [];

        // 5
        $ActionBody = [
            'action'   => 'continue',
            'step'     => EventConstants::QUESTIONS,
            'count'    => 5,
            'round_id' => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 5, 'regular');
        $button->setColumns(1);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // 10 - 50
        for ($x = 10; $x <= 50; $x+=10) {
            $ActionBody = [
                'action'   => 'continue',
                'step'     => EventConstants::QUESTIONS,
                'count'    => $x,
                'round_id' => $this->round_id
            ];
            $button = new Button('reply', json_encode($ActionBody), $x, 'regular');
            $button->setColumns(1);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Save questions count
     *
     * @return void
     */
    protected function saveQuestionsCount()
    {
        Round::updateQuestionsCount($this->message_text->round_id, $this->message_text->count);
    }

    /**
     * Compile message for finishing creating round
     * Invite friends oor continue as single player
     *
     * @return void
     */
    protected function startRound()
    {
        $this->message = 'Pozovi prijatelje u rundu ili nastavi sam.';

        $buttons = [];

        // Invite contacts
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::START,
            'type'       => 'multi',
            'round_id'   => $this->round_id
        ];
        $forward_url = 'viber://forward?text=viber://pa?chatURI=testUri%26context=' . $this->request->sender['id'];
        $button = new Button('open-url', $forward_url, 'Pozovi prijatelje...', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setSilent(true);
        $button->setBgColor('#9fd9f1');
        $button->setActionButton('forward');
        $button->setActionPredefinedURL($forward_url);
        $this->buttons[] = $button;

        // Single
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::START,
            'type'       => 'single',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Igraću sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;
    }

    /**
     * Get message
     *
     * @return \Paragraf\ViberBot\Messages\Message
     */
    public function getMessage()
    {
        if (!empty($this->buttons))
        {
            $keyboard = new Keyboard($this->buttons);
            $message = new Message('text', $keyboard);
        }
        else
        {
            $message = new Message('text', NULL);
        }

        if ($this->step_pitanje)
        {
            $message->setTrackingData(EventConstants::ONBOARDING_QUESTION);
        }
        elseif (!$this->finished)
        {
            $message->setTrackingData(EventConstants::ONBOARDING);
        }
        return $message;
    }

    /**
     * Get message text
     *
     * @return String
     */
    public function getMessageText()
    {
        return $this->message;
    }
}
