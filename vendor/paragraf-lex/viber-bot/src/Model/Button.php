<?php
/**
 * Created by PhpStorm.
 * User: nemanja.ivankovic
 * Date: 12/31/2018
 * Time: 8:09 AM.
 */

namespace Paragraf\ViberBot\Model;

class Button
{
    public $ActionType;

    public $ActionBody;

    public $Text;

    public $TextSize;

    public $Columns;

    public $Rows;

    public $BgColor;

    protected $Image;

    protected $TextHAlign;

    protected $TextVAlign;

    public $ActionButton;

    public $ActionPredefinedURL;

    public $Silent;

    public function __construct($ActionType, $ActionBody, $Text, $TextSize)
    {
        $this->ActionType = $ActionType;
        $this->ActionBody = $ActionBody;
        $this->Text = $Text;
        $this->TextSize = $TextSize;
    }

    public function getActionType()
    {
        return $this->ActionType;
    }

    public function setActionType($ActionType)
    {
        $this->ActionType = $ActionType;

        return $this;
    }

    public function getActionBody()
    {
        return $this->ActionBody;
    }

    public function setActionBody($ActionBody)
    {
        $this->ActionBody = $ActionBody;

        return $this;
    }

    public function getText()
    {
        return $this->Text;
    }

    public function setText($Text)
    {
        $this->Text = $Text;

        return $this;
    }

    public function getTextSize()
    {
        return $this->TextSize;
    }

    public function setTextSize($TextSize)
    {
        $this->TextSize = $TextSize;

        return $this;
    }

    public function setColumns($Columns)
    {
        $this->Columns = $Columns;

        return $this;
    }

    public function getColumns()
    {
        return $this->Columns;
    }

    public function setRows($Rows)
    {
        $this->Rows = $Rows;

        return $this;
    }

    public function getRows()
    {
        return $this->Rows;
    }

    public function setBgColor($BgColor)
    {
        $this->BgColor = $BgColor;

        return $this;
    }

    public function getBgColor()
    {
        return $this->BgColor;
    }

    public function setImage($Image)
    {
        $this->Image = $Image;

        return $this;
    }

    public function getImage()
    {
        return $this->Image;
    }

    public function setTextHAlign($TextHAlign)
    {
        $this->TextHAlign = $TextHAlign;

        return $this;
    }

    public function getTextHAlign()
    {
        return $this->TextHAlign;
    }

    public function setTextVAlign($TextVAlign)
    {
        $this->TextVAlign = $TextVAlign;

        return $this;
    }

    public function getTextVAlign()
    {
        return $this->TextVAlign;
    }

    public function setActionButton($ActionButton)
    {
        $this->InternalBrowser['ActionButton'] = $ActionButton;

        return $this;
    }

    public function getActionButton()
    {
        return $this->InternalBrowser['ActionButton'];
    }

    public function setActionPredefinedURL($ActionPredefinedURL)
    {
        $this->InternalBrowser['ActionPredefinedURL'] = $ActionPredefinedURL;

        return $this;
    }

    public function getActionPredefinedURL()
    {
        return $this->InternalBrowser['ActionPredefinedURL'];
    }

    public function setSilent($Silent)
    {
        $this->Silent = $Silent;

        return $this;
    }

    public function getSilent()
    {
        return $this->Silent;
    }
}
