<?php

namespace Paragraf\ViberBot\Messages;

use Paragraf\ViberBot\Http\Http;
use Paragraf\ViberBot\Intefaces\MessageInterface;
use Log;

class WelcomeMessage implements MessageInterface
{
    protected $user_id;

    protected $type;

    protected $message;

    protected $keyboard;

    /**
     * Instantiate welcome message
     *
     * @param  String                            $type      Welcome message type
     * @param  Paragraf\ViberBot\Model\Keyboard  $keyboard  Welcome message keyboard
     * @param  String                            $message   Welcome message text
     * @return string
     */
    public function __construct(String $type = 'text', \Paragraf\ViberBot\Model\Keyboard $keyboard, String $message)
    {
        $this->setType($type);
        $this->setKeyboard($keyboard);
        $this->setMessage($message);
    }

    public function body()
    {
        // picture
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'type' => 'picture',
            // 'text' => 'Dobrodošao na KvizBot!',
            'media' => config('viberbot.photo'),
            'thumbnail' => config('viberbot.thumb')
        ];

        // text
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'min_api_version' => 1,
            'type' => 'text',
            // 'text' => 'Dobrodošao na KvizBot!',
        ];

        // picture keyboard
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'type' => 'picture',
            'text' => $this->getMessage(),
            'media' => config('viberbot.photo'),
            'thumbnail' => config('viberbot.thumb'),
            'keyboard' => $this->getKeyboard()
        ];

        // text keyboard test
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'type' => 'text',
            'text' => $this->getMessage(),
            'keyboard' => [
                'Type' => 'keyboard',
                'DefaultHeight' => false,
                'Buttons' => [(object) [
                        'Columns' => 6,
                        'Rows' => 1,
                        'ActionType' => 'reply',
                        'ActionBody' => 'continue',
                        'Text' => 'Želim da probam!',
                        'TextSize' => 'regular',
                        'BgColor' => '#9fd9f1'
                    ],[
                        'Columns' => 6,
                        'Rows' => 1,
                        'ActionType' => 'reply',
                        'ActionBody' => 'instructions',
                        'Text' => 'Pokaži mi uputstvo.',
                        'TextSize' => 'regular',
                        'BgColor' => '#9fd9f1'
                    ]
                ]
            ],
        ];

        // text keyboard
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'onboarding',
            'type' => $this->getType(),
            'text' => $this->getMessage(),
            'keyboard' => $this->getKeyboard()
        ];

        return $body;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($text)
    {
        $this->message = $text;
    }

    public function getKeyboard()
    {
        return $this->keyboard;
    }

    public function setKeyboard($keyboard)
    {
        $this->keyboard = $keyboard;
    }
}
