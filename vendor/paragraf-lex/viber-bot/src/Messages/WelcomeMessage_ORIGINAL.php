<?php
/**
 * Created by PhpStorm.
 * User: nemanja.ivankovic
 * Date: 12/26/2018
 * Time: 2:17 PM.
 */

namespace Paragraf\ViberBot\Messages;

use Paragraf\ViberBot\Intefaces\MessageInterface;
use Log;

class WelcomeMessage extends Message implements MessageInterface
{
    // TODO: Welcome Message

    protected $text;

    public function body()
    {
        $body = array_merge(parent::body(), [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'tracking data welcome message',
            'type' => 'picture',
            'text' => 'Dobrodošao u KvizBot!',
            'media' => config('viberbot.photo'),
            'thumbnail' => config('viberbot.thumb')
        ]);

        unset($body['receiver']);

        //Log::info(
        //    'WelcomeMessage BODY: ' . PHP_EOL .
        //    'data: ' . PHP_EOL .print_r($body, true) . PHP_EOL
        //);

        return $body;
    }

    public function send()
    {
Log::info('Sending WelcomeMessage');
        Http::call('POST', '', $this->body());
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text): void
    {
        $this->text = $text;
    }
}
