<?php

namespace Paragraf\ViberBot;

use Paragraf\ViberBot\Http\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

use Log;

//TODO: Make it through composer | Not it's not working, no reason
require_once 'helpers.php';

class Bot
{
    // TODO: Need to make Exceptions for every part of package.

    protected $proceed = false;

    protected $request;

    protected $text;

    protected $replays = [];

    protected $body = [];

    protected $event;

    protected $question;

    public function __construct($request, $type)
    {
        $this->request = $request;
        $this->body = $type->body();
    }

    public function on($event)
    {
        if ($this->request->event === $event->getEvent()) {
            $this->event = $event;

            $this->proceed = true;
        }

        return $this;
//        throw Exception;
    }

    public function hears($text)
    {
        if (is_array($text)) {
            foreach ($text as $txt) {
                if ($this->request->message['text'] === $txt) {
                    return $this->hears($txt);
                }
            }

            $this->proceed = false;
        }

        if (is_string($text)) {

            /*
             * This represent regex.
             */
            if (startWith('/', $text) && endWith('/', $text)) {
                if (preg_match($text, $this->request->message['text'])) {
                    return $this;
                }
            }

            if ($this->request->message['text'] === $text) {
                $this->text = $text;

                return $this;
            }

            $this->proceed = false;

//          throw Exception;
        }

        return $this;
    }

    public function body($callback)
    {
        $callback(func_get_args());

        return $this;
    }

    public function replay($answer, $method = null)
    {
//Log::info('Bot::replay');
        if (is_array($answer)) {
            $this->replays = $answer;

            return $this;
        }

        if (is_string($answer)) {
            $this->replays[] = $answer;

            return $this;
        }

        if (is_a($answer, Collection::class)) {
            foreach ($answer as $item) {
                if (is_subclass_of($item, Model::class)) {
                    eval('$this->replays[] = $item->'.$method.';');
                }
            }

            return $this;
        }
    }

    public function send()
    {
//Log::info('Bot::send START');
        if ($this->proceed) {
            $result = [];
//Log::info('Bot::send proceed');
            if (count($this->replays) === 1) {
//Log::info('Bot::send one');
                $result = Http::call('POST', 'send_message', array_merge($this->body, ['text' => $this->replays[0], 'receiver' => $this->event->getUserId()]));
                $this->replays = [];
                return $result;
            }

            foreach ($this->replays as $replay) {
//Log::info('Bot::send multiple');
                $result = Http::call('POST', 'send_message', array_merge($this->body, ['text' => $replay, 'receiver' => $this->event->getUserId()]));
            }
            $this->replays = [];
//Log::info('Bot::send END');

            return $result;
        }

//        throw Exception
    }


    public function sendWelcomeResponse()
    {
        return json_encode($this->body);
    }

    // TODO: Make conversation part of bot / For the future
    /****/

//    public function conversation(Closure $conversation)
//    {
//        $conversation($conversation);
//
//        return $this;
//    }
//
//    public function ask($question/*,Closure $callback*/)
//    {
//        if (is_string($question))
//        {
//            $this->question = $question;
//
//            if ($this->proceed) {
////                $callback($question);
//                $this->replay($question)->send();
//
//                return $this;
//            }
//
//            return $this;
//        }
//
//        return;
//    }
//
//    public function say($answer)
//    {
//        $this->replay($answer)->send();
//
//        return;
//    }
}
