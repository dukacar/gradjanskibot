<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Inđijski građanski Bot</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
            xfbml            : true,
            version          : 'v7.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/sr_RS/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
        attribution=setup_tool
        page_id="106818014386395"
        theme_color="#71c55d"
        logged_in_greeting="Zdravo! Drago mi je da si zainteresovan za mene. Šta te interesuje?"
        logged_out_greeting="Zdravo! Drago mi je da si zainteresovan za mene. Šta te interesuje?">
    </div>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><span>građanski</span>BOT</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero">Početna</a></li>
          <li><a href="#sta">Šta sam ja?</a></li>
          <li><a href="#zasto">Zašto postojim?</a></li>
          <li><a href="#kako">Kako da pomogneš?</a></li>
          <li><a href="#pitanja">Šta me svi pitaju</a></li>
          <li><a href="#cena" style="color: #fa5050;">Cena</a></li>
          <li><a href="#kontakt">Kontakt</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Hero Section
  ============================-->
  <section id="hero" class="wow fadeIn">
    <div class="hero-container">
      <h1>Zdravo, ja sam inđijski Gradjanski Bot.</h1>
      <h2>Jedini BOT u službi građana.</h2>
      <img src="img/gradjanskibot.jpg" alt="Građanski BOT">
      <a href="#get-started" class="btn-get-started scrollto">Priključi se!</a>
      <div class="btns">
        <a href="#sta"><i class="fa fa-play fa-3x"></i> Šta sam ja</a>
        <a href="#zasto"><i class="fa fa-play fa-3x"></i> Zašto postojim</a>
        <a href="#pitanja"><i class="fa fa-play fa-3x"></i> Pitanja</a>
      </div>
    </div>
  </section><!-- #hero -->

  <!--==========================
    Get Started Section
  ============================-->
  <section id="get-started" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">

        <h2>Uključi se u odlučivanje u svojoj zajednici!</h2>


        Skeniraj QR kod
        <p class="separator"><img src="img/qr_indjija.png" alt="Građanski BOT"></p>

        <a href="#about-us" class="btn-get-started scrollto">Kako?</a>

        <p><a href="viber://pa?chatURI=gradjanskibot&context=indjija" class="btn-get-started scrollto">Ili me dodirni ako gledaš na mobilnom.</a></p>

      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="img/svg/pitaj.svg" alt="img" class="img-fluid">
            <h4>pitaj slobodno</h4>
            <p>Postavljaj pitanja gradskim odbornicima i oceni njihove odgovore.</p>
            <!--<a href="#">read more</a>-->

          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="img/svg/budi_pitan.svg" alt="img" class="img-fluid">
            <h4>budi pitan</h4>
            <p>Učestvuj u anketama obrornika i svojih sugrađana.</p>
            <!--<a href="#">read more</a>-->

          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="img/svg/inicijativa.svg" alt="img" class="img-fluid">
            <h4>pokreni inicijativu</h4>
            <p>Ne čekaj da ostali urade nešto za tebe, pokreni ih sam.</p>
            <!--<a href="#">read more</a>-->

          </div>
        </div>

      </div>
    </div>

  </section>

  <!--==========================
    About Us Section
  ============================-->
  <section id="about-us" class="about-us padd-section wow fadeInUp">
    <div class="container">

      <div class="section-title text-center">
        <h2>Kako?</h2>
        <p class="separator">Za sada sam samo na Viberu.</p>
        <p class="separator">Otvori listu konverzacija na telefonu i prati uputstva ispod (ako ne vidis opcije kada me koristiš pritisni dugme na slici tri).</p>
      </div>

      <div class="row justify-content-center">

        <div class="col-md-1 col-lg-4" style="text-align: center;">
          <img src="img/web_uputstvo_01.jpg" alt="About" width="277" height="600">
        </div>

        <div class="col-12 col-lg-4" style="text-align: center;">
            <img src="img/web_uputstvo_02.jpg" alt="About" width="277" height="600">
        </div>

        <div class="col-md-1 col-lg-4" style="text-align: center;">
            <img src="img/web_uputstvo_03.jpg" alt="About" width="277" height="600">
        </div>

      </div>
    </div>
  </section>

  <!--==========================
    Šta sam ja Section
  ============================-->

  <section id="sta" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Šta sam ja?</h2>
        <p class="separator">JA SAM MAŠINA + SOFTVER = ROBOT.</p>
        <p class="separator">Ja nisam klasična grupa u kojoj će te svi zatrpavati bespotrebnim informacijama.</p>
        <p class="separator">Mene ne intersuje tvoje: ime i prezime, broj telefona, e-mail adresa, političko opredeljenje, nacionalnost, religija, pol ili boja kože.</p>
      </div>
    </div>

    <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">

            <p>
                Svrha mog postojanja da budem oruđe u rukama građana sa ciljem da vrate deo moći za odlučivanje o svom gradu.
                <br>
                Služiću kao veza za informacije i odluke između svih učesnika, odbornika i građana.
            </p>
            <hr style="width:50%">
            <p>
                Anonimnost građana mi je od najvećeg značaja, za odbornike ću morati da znam samo ime i prezime.
                <br>
                Ni na koji način neću cenzurisati nikoga ali ne volim psovke i možda neću prenositi takve poruke nego ću zahtevati da se poruka preformuliše.
                <br>
                Neću sakupljati tvoje lične podatke ali ne moraš meni verovati, svaka platforma na kojoj se pojavim ima opciju sa kojom možeš da ih sakriješ od mene.
            </p>
            <hr style="width:50%">
            <p>
                Znam da nemaš ni vremana ni volje da sam juriš za informacijama, znam da imaš malo nade da se stvari mogu promeniti ali nemoj očajavati, videćeš da je sve vrlo jednostavno i moguće!
                <br>
                Dodaj me u svoje kontakte i nećeš sam morati da juriš informacije nego će informacije dolaziti do tebe, na tvoj telefon. Na Viber, Fejsbuk mesindžer, itd.
                <br>
                Za sada sam samo na Viberu ali ako me građani prihvate, ja ću se razvijati i preći i na druge platforme na koje građani svakodnevno koriste.
            </p>
            <hr style="width:50%">
            <p>
                Znam da si, vremenom, izgubio poverenje u mnoge stvari i zato sam spreman da ti odmah pokažem kako ću ti služiti.
                <br>
                <a href="#get-started" style="color: #222"><b>Skeniraj kod i uključi se!</b></a>
            </p>
            <hr style="width:50%">
            <p>
                Ako ti ne budem koristan, obriši me kao bilo koju grupu. Izaberi "delete and unsubscribe" opciju i neću te više uznemiravati.
                <br>
                Ako se kasnije predomisliš, uvek ponovo možeš skenirati QR kod i bićes dobrodošao.
            </p>

        </div>
      </div>
    </div>
  </section>

  <!--==========================
    Zasto Section
  ============================-->

  <section id="zasto" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Zašto postojim?</h2>
        <p class="separator">MOJ IDEJNI TVORAC JE GRUPA TVOJIH SUGRAĐANA.</p>
        <p class="separator">Oni smatraju da će sa mnom (kao oruđem) i zajedno sa ostalim sugrađanima rešiti neke od problema koji su vam zajednički.</p>
        <p class="separator">Njihova vizija je da gradske odbornike, građani zajedno, privedu njihovoj svrsi a misija im je da ja zaživim.</p>
        <p class="separator">I TVOJA POMOĆ IM JE POTREBNA!</p>
      </div>
    </div>

    <div class="container">
      <div class="row">

          <div class="col-sm-12 col-md-12 col-lg-12">
            <p>
                Obaveza odbornika je da zastupa tvoje interese.
                <br>
                Svaka njegova radnja treba da bude otvorena za javnost.
                <br>
                Ako tvoji interesi nisu zastupljeni imaš pravo da ga pozoveš na odgovornost!
                <br>
                <b>DA LI ZNAŠ IJEDNOG GRADSKOG ODBORNIKA?</b>
                <br>
                <br>
                Rad opštinske uprave je stvar tvog interesa i dužna je da te uključi u odlučivanje.
                <br>
                <b>DA LI TE JE NEKO IKAD PITAO ZA MIŠLJENJE?</b>
                <br>
                <br>
                Dužnost odbornika je da te informiše o svim planovima i odlukama koje utiču na tvoje interese.
                <br>
                <b>DA LI SI PRIMETIO DA IKO UOPŠTE IMA NAMERU DA TE INFORMIŠE?</b>
            </p>
            <hr style="width:50%">
            <p>
                Njihova vizija je grad u kome svi građani informisani i uključeni u donošenje odluka.
                <br>
                Misle da imaju ideju kako da to postane nova realnost, kako da natearmo odbornike da, za promenu, malo rade u interesu građana.
                <br>
                Žele da naprave sistem u kome nećeš ti morati da juriš informacije nego će informacije i ankete dolaziti do tebe, na tvoj telefon. Na Viber, Fejsbuk mesindžer, itd.
            </p>
            <hr style="width:50%">
            <p>
                Okupili su se oko ove ideje i napravili grupu koju su nazvali "<b>Nestranačke komšijske inicijative Inđija</b>". Naziv su negde videli i svideo im se a nisu hteli da gube vreme na naziv već da se fokusiraju na svoju viziju i misiju..
                <br>
                Kandiduju svoje odbornike za predstojeće izbore jer samo tako će biti sigurni da će njihova ideja nastaviti da živi i da se širi.
                <br>
                Žele da na svom primeru pokažu kako odbornici mogu da sarađuju sa građanima u cilju opšteg interesa.
                <br>
                Voleli bi da vide da vaš grad uvede direktnu demokratiju u praksi, da se građani napokon nešto pitaju.
            </p>
        </div>

      </div>
    </div>
  </section>

  <!--==========================
    Kako da pomogneš Section
  ============================-->
  <section id="kako" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Potrebna je pomoć svih građana!</h2>
        <p class="separator">Samo građani zajedno mogu da nateraju političare da rade u opštem interesu.</p>
      </div>
    </div>

    <div class="container">
      <div class="row">

          <div class="col-sm-12 col-md-12 col-lg-12">
            <p>
                Grupu građana "<b>Nestranačke komšijske inicijative Inđija</b>" je napravila listu za odbornike na predstojećim lokalnim izborima u Inđiji.
                <br>
                Oni smatraju da je demokratija i sloboda nešto najvažnije za svako društvo!
                <br>
                Ako i ti misliš isto, potrebna im je tvoja podrška jer samo tako će biti u prilici da ostvare svoju viziju.
                <br>
                Dovoljno je samo malo tvog vremena i to je sve.
            </p>
            <hr style="width:50%">
            <p>
                <ul class="list-unstyled">
                  <li><i class="fa fa-angle-right"></i> <a href="#get-started" style="color: #222"><b>Skeniraj kod i uključi se, to je dovoljno!</b></a></li>
                  <li><i class="fa fa-angle-right"></i> Podrži ih svojim potpisom za listu odbornika.</li>
                  <li><i class="fa fa-angle-right"></i> Glasaj za njihovu listu na predstojećim lokalnim izborima.</li>
                  <li><i class="fa fa-angle-right"></i> Širi dalje.</li>
                  <li><i class="fa fa-angle-right"></i> Ako želiš da pomogneš više, slobodno im se obrati i razgovaraj s njima.</li>
                </ul>
            </p>
            <hr style="width:50%">
            <p>
                <b style="color: #000; font-weight:600">Oni već sakupljaju potpise za podršku svojoj listi:</b>
                <ul class="list-unstyled">
                  <li><i class="fa fa-angle-right"></i> Sreda, 3. jun, od 17 do 20 časova.</li>
                  <li><i class="fa fa-angle-right"></i> Četvrtak 4. jun, od 17 do 20 časova.</li>
                  <li><i class="fa fa-angle-right"></i> Petak 5. jun, od 16 do 20 časova.</li>
                  <li><i class="fa fa-angle-right"></i> Subota 6. jun, od 9 do 17 časova.</li>
                </ul>
                <b style="color: #000; font-weight:600">U tržnom centar Sloboda, u lokalu 22 (preko puta kafića Versus).</b>
            </p>
        </div>

      </div>
    </div>

  </section>

  <!--==========================
    Pitanja Section
  ============================-->
  <section id="pitanja" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Odgovoriću na sva vaša pitanja.</h2>
        <p class="separator">Ako želim da i drugi budu transparentni, moram pokazat na sopstvenom primeru da je to moguće.</p>
      </div>
    </div>

    <div class="container">
      <div class="row">

          <div class="col-sm-12 col-md-12 col-lg-12">
            <p>
                <b style="color: #000; font-weight:600">Ma da li si ti to u stvari BOT "Pokreta slobodnih Građana"?</b>
                <br>
                Ne i da.
                <br>
                Moji tvorci sa pokretom slobodnih građana dele slične vrednosti, glad za demokratijom i slobodom.
                <br>
                Oni nisu vični izbornom procesu, to nije njihov fokus. Oni imaju svoju viziju i teraju svoju priču.
                <br>
                Pokret slobodnih građana im pruža neprocenjivu administrativnu podršku u izbornom procesu a za uzvrat oni im pomažu u sakupljanju potpisa za pokrajinsku i republičku listu.
                <br>
                Udružili su se jer su obe organizacije male i slabe a znaju da su zajedno jači i dele mesta na zajedničkoj lokalnoj odborničkoj listi.
            </p>
            <hr style="width:50%">
            <p>
                <b style="color: #000; font-weight:600">Ako želim da podržim lokalnu listu, da li moram da podržim i listu Pokreta slobodnih građana?</b>
                <br>
                Ne.
                <br>
                Slobodan si i tvoj izbor je samo tvoj i ostali moraju da ga poštuju. Čak i ako želiš da podržiš listu Pokreta slobodnih građana a ne i lokalnu i to je skroz u redu.
            </p>
            <hr style="width:50%">
            <p>
                <b style="color: #000; font-weight:600">Zašto misliš da će se i ostali odbornici priključiti?</b>
                <br>
                Znam da si izgubio poverenje u političare jer su te izneverili svaki put i da ne možeš da se osloniš na njihovu čestitost.
                <br>
                Ja ne računam na njihovu saradnju. ono na šta ja računam (jer sam robot, samo to i radim) je da će slediti svoj lični interes kao što uvek i nepogrešivo vi ljudi to i radite.
                <br>
                Ako vi građani, izglasate da neki ljudi sa lokalne liste postanu odbornici, prvi korak u dobrom pravcu je učinjen.
                <br>
                Njihov lični interes je da građane, kao opoziciona manjina, informišu i pitaju kako bi zadobili vaše poverenje i vaše glasove, da povećaju svoju popularnost.
                <br>
                Zatim će, vrlo brzo i ostali odbornici shvatiti da je ovo odličan način za njihovu ličnu promociju i promociju njihovih političkih partija.
                <br>
                Ako se to desi, vi građani ste već pobedili jer više nema nazad. Kako će neko da objasni zašto neki odbornici mogu da uključe građane u odlučivanje a ti, moj odbornik, kome sam dao povernje potpisom, za koga sam glasao ne?
                <br>
                Dakle, što više građana se priključi, uveri se kako ću im služiti, pomogne lokalnu listu da dobiju odbornička mesta i glasa za njih, šanse za ostvarenje ove misije su veći.
            </p>
            <hr style="width:50%">
            <p>
                <b style="color: #000; font-weight:600">Da li mogu i ostale liste da se priključe odmah?</b>
                <br>
                Da.
                <br>
                Platforma je građanska, potpuno otvorena za sve koji žele da je koriste.
            </p>
        </div>

      </div>
    </div>

  </section>

  <!--==========================
    Video Section
  ============================-->

  <!--
  <section id="video" class="text-center wow fadeInUp">
    <div class="overlay">
      <div class="container-fluid container-full">

        <div class="row">
          <a href="#" class="js-modal-btn play-btn" data-video-id="s22ViV7tBKE"></a>
        </div>

      </div>
    </div>
  </section>
  -->

  <!--==========================
    Pricing Table Section
  ============================-->
  <section id="cena" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">

        <h2>Zauvek besplatan!</h2>
        <p class="separator">Napravljen od građana za građane i u vlasništvu je građana.</p>

      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing">
            <div class="table">
              <h4>građani</h4>
              <h2>RDS 0</h2>
              <ul class="list-unstyled">
                <li><b>Treba ti:</b></li>
                <li>Telefon</li>
                <li>Internet</li>
                <li>Dobra volja</li>
                   <li>Sugrađani i odbornik</li>
              </ul>
              <div class="table_btn" style="padding-top: 30px; margin-bottom: 15px;">
                <a href="#about-us" class="btn"><i class="fa fa-shopping-cart"></i> Priključi se!</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing">
            <div class="table">
              <h4>odbornici</h4>
              <h2>RDS 0</h2>
              <ul class="list-unstyled">
                <li><b>Treba ti:</b></li>
                <li>Telefon</li>
                <li>Internet</li>
                <li>Dobra volja</li>
                <li>Građani</li>
              </ul>
              <div class="table_btn" style="padding-top: 30px; margin-bottom: 15px;">
                <a href="#about-us" class="btn"><i class="fa fa-shopping-cart"></i> Priključi se!</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing">
            <div class="table">
              <h4>grupe građana</h4>
              <h2>RDS 0</h2>
              <ul class="list-unstyled">
                <li><b>Treba ti:</b></li>
                <li>Telefon</li>
                <li>Internet</li>
                <li>Dobra volja</li>
                <li>Sugrađani</li>
              </ul>
              <div class="table_btn" style="padding-top: 30px; margin-bottom: 15px;">
                <a href="#about-us" class="btn"><i class="fa fa-shopping-cart"></i> Priključi se!</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing">
            <div class="table">
              <h4>političke partije</h4>
              <h2>RDS 0</h2>
              <ul class="list-unstyled">
                <li><b>Treba ti:</b></li>
                <li>Telefon</li>
                <li>Internet</li>
                <li>Dobra volja</li>
                <li>Kolege</li>
              </ul>
              <div class="table_btn" style="padding-top: 30px; margin-bottom: 15px;">
                <a href="#about-us" class="btn"><i class="fa fa-shopping-cart"></i> Priključi se!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--==========================
    Kontakt Section
  ============================-->
  <section id="kontakt" class="padd-section wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Kontakt</h2>
        <!--<p class="separator">Ako želim da i drugi budu transparentni, moram pokazat na sopstvenom primeru da je to moguće.</p>-->
      </div>
    </div>

    <div class="container text-center">
      <div class="row">

          <div class="col-sm-12 col-md-12 col-lg-12">
            <p>
                Pošto sam ja robot i egzistiram u oblaku, fizčki ne možeš doći do mene ali se priključi i biću ti dostupan na dodir prsta.
            </p>
            <hr style="width:50%">
            <p>
                Takođe imam svoju Fejsbuk stranu: <a target="_blank" href="https://www.facebook.com/Inđijski-građanski-Bot-106818014386395"><i class="fa fa-facebook"></i></a>
                <br>
                Ako me građani prihvate, lako ću se proširiti kako na Fejsbook mesindžer, tako i na Twiter i ostale platforme na kojima građani budu želeli da me vide.
            </p>
            <hr style="width:50%">
            <p>
                Ako pak želiš da porazgovaraš sa nekim od krvi i mesa, slobodno se obrati ljudima iz Nestranačkih komšijskih inicijativa iz Inđije.
                <br>
                <a href="#kako" style="color: #000;"><b>Gde i kada?</b></a>
            </p>
            <hr style="width:50%">
            <p>
                Možeš pisati na: <b><span id="botmail"></span></b> i daću sve od sebe da ti brzo odgovorim.
                <script>
                    var parts = ["automatica", "rs", "gradjanskibot", "&#46;", "&#64;"];
                    var botmail = parts[2] + parts[4] + parts[0] + parts[3] + parts[1];
                    document.getElementById("botmail").innerHTML = botmail;
                </script>
            </p>
        </div>

      </div>
    </div>

  </section>

  <!--==========================
    Footer
  ============================-->
  <footer class="footer">
    <div class="container">
      <div class="row">

        <div class="col-md-12 col-lg-4">
          <div class="footer-logo">

            <span class="navbar-brand" href="#">građanskiBOT</span>
            <p>Smišljen sam i kreiram da pomognem građanima da ostvare ideal direktne demokratije. Da budu pitani za mišljenje u svojoj lokalnoj zajednici.</p>

          </div>
        </div>

        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero">Početna</a></li>
          <li><a href="#sta" style="color: #BBB;">Šta sam ja?</a></li>
          <li><a href="#zasto" style="color: #BBB;">Zašto postojim?</a></li>
          <li><a href="#kako" style="color: #BBB;">Kako da pomogneš?</a></li>
          <li><a href="#pitanja" style="color: #BBB;">Šta me svi pitaju</a></li>
          <li><a href="#cena" style="color: #fa5050;">Cena</a></li>
          <li><a href="#kontakt" style="color: #BBB;">Kontakt</a></li>
        </ul>

      </div>
    </div>

    <div class="copyrights">
      <div class="container">
        <p>&copy; 2020. građanskiBOT.</p>
    </div>

  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/modal-video/js/modal-video.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
